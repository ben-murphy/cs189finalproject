# README #

This README would normally document whatever steps are necessary to get your application up and running.
Loosely based on the following paper: https://www.nature.com/articles/s41598-017-18363-1

### What is this repository for? ###

This is a final project for Eddie Gao and Ben Murphy. We're testing whether or not the incorporating the distance of neurons from each other
as well as individualized activation functions helps the performance of a neural net.

Please feel free to check out the final paper (FinalPaper.pdf) for more information and results!
